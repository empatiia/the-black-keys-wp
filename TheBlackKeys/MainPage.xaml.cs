﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using TheBlackKeys.DataSources;
using TheBlackKeys.Objects;
using System.IO.IsolatedStorage;
using TheBlackKeys.Strings;
using TheBlackKeys.Managers;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace TheBlackKeys
{
    public partial class MainPage : PhoneApplicationPage
    {
        private ArtistList _artists = null;
        private AlbumList _albums   = null;
        private IEnumerable<Tour> _tours = null;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            this.LoadInterface();

            this.Loaded += new RoutedEventHandler(MainPage_Loaded);
            
        }

        private void PlaySong()
        {
            var mediaElement = new MediaElement();
            mediaElement.Source = new Uri("Data/song.mp3", UriKind.Relative);
            mediaElement.Position = new TimeSpan(0);
            LayoutRoot.Children.Add(mediaElement); //Add to visual tree
            mediaElement.Play();
        }

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            //this.ShowWelcomeMessage();
            PlaySong();
        }

        private void LoadInterface()
        {
            _artists = new ArtistList();
            _albums = new AlbumList();
            _tours = TourManager.getAllTours() as IEnumerable<Tour>;
            (App.Current as App).ArtistListList = _artists;
            (App.Current as App).AlbumListList = _albums;
            (App.Current as App).TourListList = _tours;
            listBoxArtists.ItemsSource = _artists;
            listBoxAlbums.ItemsSource = _albums;
            listBoxDates.ItemsSource = _tours;

            todayText.Text = TheBlackKeys.Strings.AppResources.TODAY + " " + String.Format("{0:dddd dd MMMM yyyy}", DateTime.Now);
        }

        private void ShowWelcomeMessage()
        {
            IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;

            if (appSettings.Count == 0 || (string)appSettings["welcome"] == null)
            {
                MessageBox.Show(TheBlackKeys.Strings.AppResources.WELCOME);
                appSettings.Add("welcome", "1");
            }
        }

        private void StackPanel_Tap_1(object sender, GestureEventArgs e)
        {
            if (listBoxArtists.SelectedIndex >= 0)
                NavigationService.Navigate(new Uri("/Pages/ArtistPage.xaml?artist=" + listBoxArtists.SelectedIndex, UriKind.Relative));
        }

        private void listBoxAlbums_Tap(object sender, GestureEventArgs e)
        {
            if (listBoxAlbums.SelectedIndex >= 0)
                NavigationService.Navigate(new Uri("/Pages/AlbumPage.xaml?album=" + listBoxAlbums.SelectedIndex, UriKind.Relative));
        }

        private void StackPanel_Tap_2(object sender, GestureEventArgs e)
        {
            if (listBoxDates.SelectedIndex >= 0)
                NavigationService.Navigate(new Uri("/Pages/TourMapPage.xaml?tour=" + listBoxDates.SelectedIndex, UriKind.Relative));
        }
    }
}