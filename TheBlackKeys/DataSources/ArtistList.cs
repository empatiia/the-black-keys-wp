﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheBlackKeys.Objects;

namespace TheBlackKeys.DataSources
{
    public class ArtistList : List<Artist>
    {
        public ArtistList()
        {
            Add(new Artist 
            {
                Name        = "Dan Auerbach", 
                Role        = "The Singer", 
                BirthDate   = "May 14, 1979", 
                BirthPlace  = "Akron, Ohio, United States",
                Link        = "http://en.wikipedia.org/wiki/Dan_Auerbach",
                Photo       = "http://www.nonesuch.com/files/imagecache/section-artists-image/media/images/dan-auerbach-vert-hat-by-james-quine.jpg",
                Description = "Daniel Quine Dan Auerbach (born May 14, 1979) is an American musician. A multi-instrumentalist, he is best known as the guitarist and vocalist for The Black Keys, a blues rock band from Akron, Ohio. He is married to Stephanie Gonis, with whom he has a daughter, Sadie Little Auerbach, born in 2007.", 
            });
            
            Add(new Artist 
            { 
                Name        = "Patrick Carney", 
                Role        = "The Drummer",
                BirthDate   = "April 15, 1980",
                BirthPlace  = "Akron, Ohio, United States.",
                Link        = "http://en.wikipedia.org/wiki/Patrick_Carney",
                Photo       = "http://userserve-ak.last.fm/serve/500/22060029/Patrick+Carney+Patrick.jpg",
                Description = "Patrick J. Carney (born April 15, 1980) is a multi-instrumentalist best known as the drummer for The Black Keys, a blues-rock band from Akron, Ohio. In 2009, he created the rock band Drummer and released their debut album Feel Good Together on his Audio Eagle Records label. In 2012, he produced the self-titled album for Canadian rockers The Sheepdogs. Atlantic Records released it Sept. 4 of that year. In 2011, he produced the second Tennis album, Young & Old, which was released on Fat Possum Records on Feb 14, 2012. The Black Keys released three albums - Thickfreakness, Rubber Factory and Chulahoma - on Fat Possum.", 
            });
        }
    }
}
