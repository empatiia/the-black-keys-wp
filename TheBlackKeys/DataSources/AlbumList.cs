﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheBlackKeys.Objects;

namespace TheBlackKeys.DataSources
{
    public class AlbumList : List<Album>
    {
        public AlbumList()
        {
            Add(new Album 
            {
                Name        = "El Camino",
                ReleaseDate = "December 6, 2011",
                CoverPhoto  = "http://upload.wikimedia.org/wikipedia/en/9/92/The_Black_Keys_El_Camino_Album_Cover.jpg",
                Link        = "http://en.wikipedia.org/wiki/El_Camino_(The_Black_Keys_album)",
                Description = "El Camino is the seventh studio album by American rock band The Black Keys. It was co-produced by Danger Mouse and the band, and was released on December 6, 2011 on Nonesuch Records.[3] The album was recorded in Nashville, Tennessee at Easy Eye Sound Studio, which was opened in 2010 by guitarist Dan Auerbach. The record draws influences from popular genres of the 1950s–1970s, such as rock and roll, glam rock, rockabilly, surf rock, and soul. Based on the difficulty the group had performing some of the slower songs from their previous album Brothers live, it conceived more uptempo tracks for El Camino. Danger Mouse contributed as a co-writer on each of the 11 songs.",
                SongList    = generateCaminoSongs(),
            });

            Add(new Album
            {
                Name        = "Brothers",
                ReleaseDate = "May 18, 2010",
                CoverPhoto  = "http://upload.wikimedia.org/wikipedia/en/9/93/The_Black_Keys_-_Brothers.jpg",
                Link        = "http://en.wikipedia.org/wiki/Brothers_(The_Black_Keys_album)",
                Description = "Brothers is the sixth studio album by American rock duo The Black Keys. Co-produced by the group, Mark Neill, and Danger Mouse, it was released on May 18, 2010 on Nonesuch Records. Brothers was the band's commercial breakthrough, as it sold over 73,000 copies in the United States in its first week and peaked at number three on the Billboard 200, their best performance on the chart to that point. The album's lead single, 'Tighten Up', the only track from the album produced by Danger Mouse, became their most successful single to that point, spending 10 weeks at number one on the Alternative Songs chart and becoming the group's first single on the Billboard Hot 100, peaking at number 87 and was later certified gold. The second single Howlin' for You went gold as well.[3] In April 2012, the album was certified platinum in the US by the RIAA for shipping over one million copies.[4] It also went double-platinum in Canada and gold in the UK.[5]In 2011, it won three Grammy Awards, including honors for Best Alternative Music Album.",
                SongList    = generateBrothersSongs(),
            });

            Add(new Album
            {
                Name        = "Attack & Release",
                ReleaseDate = "April 1, 2008",
                CoverPhoto  = "http://upload.wikimedia.org/wikipedia/en/5/50/BlackKeys-Attack%26Release.png",
                Link        = "http://en.wikipedia.org/wiki/Attack_%26_Release",
                Description = "Attack & Release is the fifth studio album by American rock band The Black Keys. It was produced by Danger Mouse and was released on April 1, 2008. The sessions saw the band transitioning away from their 'homemade' ethos to record-making; not only was it the first time that the band completed an album in a professional studio, but it was also the first time they hired an outside producer to work on a record.",
                SongList    = generateAttackAndReleaseSongs(),
            });

            Add(new Album
            {
                Name        = "Magic Potion",
                ReleaseDate = "September 12, 2006",
                CoverPhoto  = "http://upload.wikimedia.org/wikipedia/en/b/b7/The_Black_Keys_-_Magic_Potion.jpg",
                Link        = "http://en.wikipedia.org/wiki/Magic_Potion_(album)",
                Description = "Magic Potion is the fourth studio album by blues rock duo The Black Keys. It was released in 2006 and was their first record released on Nonesuch Records, the band's current label. This album marks the first time The Black Keys wrote and composed entirely original material, unlike on previous albums and EPs.",
                SongList    = generateMagicPotionsSongs(),
            });

            Add(new Album
            {
                Name        = "Rubber Factory",
                ReleaseDate = "September 7, 2004",
                CoverPhoto  = "http://upload.wikimedia.org/wikipedia/en/a/ae/The_Black_Keys_-_Rubber_Factory.jpg",
                Link        = "http://en.wikipedia.org/wiki/Rubber_Factory",
                Description = "Rubber Factory is the third studio album by American rock band The Black Keys. It was self-produced by the band and was released on September 7, 2004. The album was recorded in an abandoned tire-manufacturing factory in the group's hometown of Akron, Ohio. Rubber Factory received positive reviews and was the band's first album to chart on the Billboard 200 in the United States, reaching number 143.",
                SongList    = generateRubberFactorySongs(),
            });

            Add(new Album
            {
                Name        = "Thickfreakness",
                ReleaseDate = "April 8, 2003",
                CoverPhoto  = "http://upload.wikimedia.org/wikipedia/en/1/1e/The_Black_Keys_-_Thickfreakness.jpg",
                Link        = "http://en.wikipedia.org/wiki/Thickfreakness",
                Description = "Thickfreakness is the second album by American blues-rock duo The Black Keys, released in 2003. It is their debut release for the Fat Possum record label.",
                SongList    = generateThickfreaknessSongs(),
            });

            Add(new Album
            {
                Name        = "The Big Come Up",
                ReleaseDate = "May 14, 2002",
                CoverPhoto  = "http://upload.wikimedia.org/wikipedia/en/f/fb/The_Black_Keys_-_The_Big_Come_Up.jpg",
                Link        = "http://en.wikipedia.org/wiki/The_Big_Come_Up",
                Description = "The Big Come Up is the debut album by the American rock duo The Black Keys, released in 2002 on Alive Records. It was produced by drummer Patrick Carney. According to Nielsen Soundscan, the record has sold 139,000 copies. In 2005, music critic Chuck Klosterman singled out The Big Come Up as one of 21 'high-album albums' from the previous three years.",
                SongList    = generateTheBigComeUpSongs(),
            });
        }

        private List<Song> generateCaminoSongs()
        {
            List<Song> songsList = new List<Song>();
            songsList.Add(new Song("Lonely Boy", "3:13", "http://www.youtube.com/watch?v=a_426RiwST8"));
            songsList.Add(new Song("Dead and Gone", "3:41", ""));
            songsList.Add(new Song("Gold on the Ceiling", "3:44", ""));
            songsList.Add(new Song("Little Black Submarines", "4:11", ""));
            songsList.Add(new Song("Money Maker", "2:57", ""));
            songsList.Add(new Song("Run Right Back", "3:17", ""));
            songsList.Add(new Song("Sister", "3:25", ""));
            songsList.Add(new Song("Hell of a Season", "3:45", ""));
            songsList.Add(new Song("Stop Stop", "3:25", ""));
            songsList.Add(new Song("Nova Baby", "3:27", ""));
            songsList.Add(new Song("Mind Eraser", "3:15", ""));
            return songsList;
        }

        private List<Song> generateBrothersSongs()
        {
            List<Song> songsList = new List<Song>();
            songsList.Add(new Song("No songs yet", "", ""));
            return songsList;
        }

        private List<Song> generateAttackAndReleaseSongs()
        {
            List<Song> songsList = new List<Song>();
            songsList.Add(new Song("No songs yet", "", ""));
            return songsList;
        }

        private List<Song> generateMagicPotionsSongs()
        {
            List<Song> songsList = new List<Song>();
            songsList.Add(new Song("No songs yet", "", ""));
            return songsList;
        }

        private List<Song> generateRubberFactorySongs()
        {
            List<Song> songsList = new List<Song>();
            songsList.Add(new Song("No songs yet", "", ""));
            return songsList;
        }

        private List<Song> generateThickfreaknessSongs()
        {
            List<Song> songsList = new List<Song>();
            songsList.Add(new Song("No songs yet", "", ""));
            return songsList;
        }

        private List<Song> generateTheBigComeUpSongs()
        {
            List<Song> songsList = new List<Song>();
            songsList.Add(new Song("No songs yet", "", ""));
            return songsList;
        }
    }
}
