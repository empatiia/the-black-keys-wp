﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TheBlackKeys.Objects;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Microsoft.Phone.Tasks;

namespace TheBlackKeys.Pages
{
    public partial class ArtistPage : PhoneApplicationPage
    {
        private Artist selectedArtist = null;
        public ArtistPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string artistIndexString = null;
            NavigationContext.QueryString.TryGetValue("artist", out artistIndexString);

            if (artistIndexString != null)
            {
                int selectedIndex = Convert.ToInt32(artistIndexString);
                this.selectedArtist = (App.Current as App).ArtistListList.ElementAt(selectedIndex);
                artistNameText.Text = this.selectedArtist.Name;
                artistRoleText.Text = this.selectedArtist.Role;
                Uri imageUri = new Uri(this.selectedArtist.Photo);
                artistPhoto.Source = new BitmapImage(imageUri);
                artistDescriptionText.Text = this.selectedArtist.Description;
                artistInfoText.Text = "Born the " + this.selectedArtist.BirthDate + "\nin " + this.selectedArtist.BirthPlace;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (this.selectedArtist != null)
            {
                WebBrowserTask browser = new WebBrowserTask();
                browser.Uri = new Uri(this.selectedArtist.Link, UriKind.Absolute);
                browser.Show();
            }
        }
    }
}