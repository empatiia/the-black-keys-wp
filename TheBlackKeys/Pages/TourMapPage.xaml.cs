﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Device.Location;
using Microsoft.Phone.Controls.Maps;
using TheBlackKeys.Objects;

namespace TheBlackKeys.Pages
{
    public partial class TourMapPage : PhoneApplicationPage
    {
        private Tour selectedTour;
        private GeoCoordinateWatcher myCoordinateWatcher;

        public TourMapPage()
        {
            InitializeComponent();

            map1.ZoomBarVisibility = Visibility.Visible;
            myCoordinateWatcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
            myCoordinateWatcher.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(myCoordinateWatcher_PositionChanged);
        }

        void myCoordinateWatcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            if (!e.Position.Location.IsUnknown)
            {
                Pushpin pin = new Pushpin();
                pin.Location.Latitude = e.Position.Location.Latitude;
                pin.Location.Longitude = e.Position.Location.Longitude;
                map1.Children.Add(pin);
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string tourIndexString = null;
            NavigationContext.QueryString.TryGetValue("tour", out tourIndexString);

            if (tourIndexString != null)
            {
                int selectedIndex = Convert.ToInt32(tourIndexString);
                this.selectedTour = (App.Current as App).TourListList.ElementAt(selectedIndex);

                Pushpin pin = new Pushpin();
                pin.Location = this.selectedTour.TourLocation;
                map1.Children.Add(pin);

                map1.Center = this.selectedTour.TourLocation;
            }
        }
    }
}