﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TheBlackKeys.Objects;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Tasks;

namespace TheBlackKeys.Pages
{
    public partial class AlbumPage : PhoneApplicationPage
    {
        public Album selectedAlbum = null;

        public AlbumPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string albumIndexString = null;
            NavigationContext.QueryString.TryGetValue("album", out albumIndexString);

            if (albumIndexString != null)
            {
                int selectedIndex = Convert.ToInt32(albumIndexString);
                this.selectedAlbum = (App.Current as App).AlbumListList.ElementAt(selectedIndex);
                albumNameText.Text = this.selectedAlbum.Name;
                releaseDateText.Text = this.selectedAlbum.ReleaseDate;
                Uri imageUri = new Uri(this.selectedAlbum.CoverPhoto);
                albumPhoto.Source = new BitmapImage(imageUri);
                albumDescriptionText.Text = this.selectedAlbum.Description;
                listBoxSongs.ItemsSource = selectedAlbum.SongList;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (this.selectedAlbum != null)
            {
                WebBrowserTask browser = new WebBrowserTask();
                browser.Uri = new Uri(this.selectedAlbum.Link, UriKind.Absolute);
                browser.Show();
            }
        }

        private void StackPanel_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
           // MessageBox.Show((this.selectedAlbum.SongList.ElementAt(0) as Song).Link);
            if (this.selectedAlbum != null &&
                !(this.selectedAlbum.SongList.ElementAt(0) as Song).Link.Equals(""))
            {
                WebBrowserTask browser = new WebBrowserTask();
                browser.Uri = new Uri((this.selectedAlbum.SongList.ElementAt(0) as Song).Link, UriKind.Absolute);
                browser.Show();
            }
        }
    }
}