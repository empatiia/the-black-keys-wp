﻿using Microsoft.Phone.Controls.Maps.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using TheBlackKeys.Objects;

namespace TheBlackKeys.Managers
{
    public static class TourManager
    {
        public static object getAllTours()
        {
            /*
             * http://www.windowsphonegeek.com/tips/wp7-working-with-xml-reading-filtering-and-databinding
             */
            XDocument loadedData = XDocument.Load("Data/tour.xml");
            var data = from query in loadedData.Descendants("concert")
                       select new Tour
                       {
                           TourDate = TourManager.parseDateFromString((string)query.Attribute("date").Value),
                           TourLocation = TourManager.parseLocationFromString((string)query.Attribute("coordinates").Value),
                           Hall = (string)query.Attribute("hall").Value
                       };

            return data;
        }

        /*
         * Dates come from XML as day/month/year
         */
        public static DateTime parseDateFromString(string dateString)
        {
            string[] splittedDate = dateString.Split('/');
            DateTime date = new DateTime(Convert.ToInt32(splittedDate[2]), // YEAR
                                                     Convert.ToInt32(splittedDate[1]), // MONTH
                                                     Convert.ToInt32(splittedDate[0])); // DAY
            return date;
        }

        /*
         * Coordinates come from XML as latitude;longitude
         */
        public static Location parseLocationFromString(string locationString)
        {
            string[] splittedLocation = locationString.Split(';');
            Location location = new Location();
            location.Latitude = Convert.ToDouble(splittedLocation[0]); // LATITUDE
            location.Longitude = Convert.ToDouble(splittedLocation[1]); // LONGITUDE

            return location;
        }
    }
}
