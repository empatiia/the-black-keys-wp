﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheBlackKeys.Objects
{
    public class Song
    {
        public string Name      { get; set; }
        public string Duration  { get; set; }
        public string Link      { get; set; }

        public Song(string name, string duration, string link)
        {
            this.Name       = name;
            this.Duration   = duration;
            this.Link       = link;
        }
    }
}
