﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheBlackKeys.Objects
{
    public class Album
    {
        public string Name          { get; set; }
        public string ReleaseDate   { get; set; }
        public string CoverPhoto    { get; set; }
        public string Link          { get; set; }
        public string Description   { get; set; }
        public List<Song> SongList  { get; set; }
    }
}
