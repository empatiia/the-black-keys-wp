﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Phone.Controls.Maps.Platform;

namespace TheBlackKeys.Objects
{
    public class Tour
    {
        public DateTime TourDate { get; set; }
        public Location TourLocation    { get; set; }
        public string Hall              { get; set; }
    }
}
