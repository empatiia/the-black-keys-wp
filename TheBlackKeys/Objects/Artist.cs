﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheBlackKeys.Objects
{
    public class Artist
    {
        public string Name          { get; set; }
        public string Role          { get; set; }
        public string BirthDate     { get; set; }
        public string BirthPlace    { get; set; }
        public string Link          { get; set; }
        public string Photo         { get; set; }
        public string Description   { get; set; }
    }
}
