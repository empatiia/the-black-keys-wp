﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheBlackKeys
{
    public class LocalizedStrings
    {
        public LocalizedStrings()
        {
        }

        private static TheBlackKeys.Strings.AppResources localizedResources = new TheBlackKeys.Strings.AppResources();

        public TheBlackKeys.Strings.AppResources LocalizedResources { get { return localizedResources; } }
    }
}
